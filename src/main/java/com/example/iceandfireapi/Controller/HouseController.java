package com.example.iceandfireapi.Controller;

import com.example.iceandfireapi.HouseRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

import static com.example.iceandfireapi.retireveFromREST.retrieve;

@RestController
@RequestMapping("/api/v1/houses")
public class HouseController {

    @Autowired
    private HouseRepository houseRepository;

    @GetMapping("/Targaryen/Titles")
    public ResponseEntity<List<String>> getTitles(){
        List<String> titles =houseRepository.getTargaryenTitles();

        return new ResponseEntity<List<String>>(titles, HttpStatus.OK);

    }


    @GetMapping()
    public ResponseEntity<List<String>>getAllHouses(){
        List<String> houses = houseRepository.getHouses();
        return new ResponseEntity<List<String>>(houses, HttpStatus.OK);
    }


    @GetMapping("/{id}")
    public ResponseEntity<JSONObject> getHouse(@PathVariable Long id){
        JSONObject object = houseRepository.getHouse(id);
        return new ResponseEntity<JSONObject>(object, HttpStatus.OK);
    }



}
