package com.example.iceandfireapi;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

import static com.example.iceandfireapi.retireveFromREST.*;

@Component
public class HouseRepository {
    private List<String> titles = new ArrayList<>();

    public List<String> getTargaryenTitles(){
        StringBuffer output = retrieve("https://anapioficeandfire.com/api/houses/378");
        JSONObject titlesObject = new JSONObject(output.toString());
        JSONArray objectArray = titlesObject.getJSONArray("titles");
        for (int i = 0; i < objectArray.length(); i++) {
            titles.add(objectArray.getString(i));
        }
        return titles;


    }

    public List<String> getHouses(){
        List<String> houses = new ArrayList<>();
        String url = "https://anapioficeandfire.com/api/houses";
        while(url != null) {
            StringBuffer output = retrieve(url);
            JSONArray object = new JSONArray(output.toString());
            JSONObject objectInArray = new JSONObject();
            for (int i = 0; i < object.length(); i++) {
                objectInArray = object.getJSONObject(i);
                houses.add(objectInArray.getString("name"));
            }
            url = nextLink(url);

        }
        return houses;

    }

    public JSONObject getHouse(Long id){
        StringBuffer output = retrieve("https://anapioficeandfire.com/api/houses" + "/" + id);

        JSONObject object = new JSONObject(output.toString());
        return object;
    }



}
