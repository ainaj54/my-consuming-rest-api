package com.example.iceandfireapi;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;

public class retireveFromREST {

    public static StringBuffer retrieve(String inputUrl){
        try{
        URL url = new URL(inputUrl);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        if(con.getResponseCode() == HttpURLConnection.HTTP_OK){
            InputStreamReader reader = new InputStreamReader(con.getInputStream());
            BufferedReader bufferReader = new BufferedReader(reader);
            String inputLine;
            StringBuffer content = new StringBuffer();
            while((inputLine = bufferReader.readLine()) != null){
                content.append(inputLine);
            }
            bufferReader.close();
            return content;
        }
        return null;

    }catch (Exception e){
        e.printStackTrace();
        }

        return null;
    }


    public static String nextLink(String inputUrl){
        try{
            URL url = new URL(inputUrl);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            String linkHeader = con.getHeaderField("Link");

            for (int i = 0; i < linkHeader.split(",").length; i++) {
                if(linkHeader.split(",")[i].contains("rel=\"next\"")){
                    System.out.println(linkHeader.split(",")[i]);
                    String stringToParse = linkHeader.split(",")[i];
                    String nextURl = stringToParse.substring(stringToParse.indexOf("<") +1, stringToParse.indexOf(">"));
                    return nextURl;
                }
            }
            return null;
            } catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

}
