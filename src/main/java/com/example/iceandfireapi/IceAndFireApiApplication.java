package com.example.iceandfireapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IceAndFireApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(IceAndFireApiApplication.class, args);

    }




}
